#include <algebra.h>

double dot(const double* A, const double* B, const int n){

  double s=0.0;
  for(int i=0; i<n; ++i) {
    s += A[i]*B[i];
  }
  return s;
}

double dDot(const int n, const double * A, const int stride_a, const double * B , const int stride_b){

  return dot (A, B, n);
  
}
