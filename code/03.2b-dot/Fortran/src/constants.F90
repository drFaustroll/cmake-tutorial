module constants
  use iso_fortran_env, only : real64
  implicit none
  private
  integer, parameter, public  :: wp = real64
end module constants
