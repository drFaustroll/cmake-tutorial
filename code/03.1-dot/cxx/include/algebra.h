#ifndef ALGEBRA_H
#define ALGEBRA_H
#include <dll_export.h>

DLL_EXPORT double dot(const double*, const double*, const int); 
DLL_EXPORT double dDot(const int n, const double * A, const int stride_a, const double * B , const int stride_b);

#endif //ALGEBRA_H
