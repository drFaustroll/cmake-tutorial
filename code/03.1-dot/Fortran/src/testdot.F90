program testDOT
  use constants, only : wp
  use algebra, only : mydot
#ifdef _OPENMP
  use omp_lib
#endif
  implicit none
  integer :: i,n,NLoops
  character(len=32) :: arg
  real(kind=wp), parameter :: k_pi = atan(1.0_wp)*4.0_wp
  real(kind=wp), allocatable :: a(:),b(:),pi(:),diff(:)
  integer :: s

  if (command_argument_count() /=2) then
    write(*,'(a)')"Wrong usage!!!"
    write(*,'(a)')"Correct usage: "
    call get_command_argument(0, arg)
    write(*,'(a)')trim(arg)//" <n>  <NRepetitions>"
    write(*,'(a)')trim(arg)//" 1000 5"
    stop
  endif

  call get_command_argument(1, arg)
  read(arg,*) n
  call get_command_argument(2, arg)
  read(arg,*) NLoops
  write(*,'(a)',advance='no')"Running... "
  do i=0,command_argument_count()
    call get_command_argument(i, arg)
    write(*,'(a,1x)',advance="no") trim(arg)
  enddo
  write(*,*)
#ifdef _OPENMP
!$omp parallel
!$omp master
  write(*,'(a,i0,a,i0)')"Using ",omp_get_num_threads() ,&
    " threads out of a maximum of ",omp_get_max_threads()
!$omp end master
!$omp end parallel
#endif


  allocate(a(n+NLoops),b(n+NLoops))
  allocate(diff(NLoops),pi(NLoops))

  s = 1
  do i = 1, n+NLoops
    a(i) = 4.0_wp/real(2*i-1,wp)
    b(i) = real(s,wp)
    s = s * (-1)
  end do

  do i=1,NLoops
    pi(i) = mydot(a(1:n+i-1),b(1:n+i-1),n+i-1)
    diff(i) = k_pi - pi(i)
  end do

  write(*,'(a8,a12,3a17)')"#Loop  |","       Size|","               pi|","          diff|","        bound|"
  do i=1,NLoops
    write(*,'(i6,1x,i12,1x,4(f16.8,1x))')i,n+i-1,pi(i),diff(i), 4.0_wp/real(2*(i+n)+1,wp)
  enddo
  write(*,*)
  write(*,'(a,es16.8)')"Last value of pi: ",pi(NLoops)
  write(*,*)
  deallocate(a,b,diff,pi)

end program testDOT
