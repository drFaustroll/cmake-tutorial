#include <iostream>
#include "dll_export.h"



DLL_EXPORT int print_stats(int n, int nSamples, double *times, double *pi, double *diff, double tmin, double tmax){
    double timeAvg = 0.0;
    for(int i=0; i<nSamples; ++i) {
      timeAvg+=times[i];
    }
    timeAvg/=nSamples;
  
    double sig=0.0;
    for(int i=0; i<nSamples; ++i) {
      sig+=(times[i]-timeAvg)*(times[i]-timeAvg);
    }
    sig=sqrt(sig/nSamples);
  
    std::cout<<"#Loop  |       Size|        Time (s)|"<<std::endl;
    for(int i=0; i<nSamples; ++i) {
      // std::cout<<i+1<<" "<<n<<" "<<times[i]<<std::endl;
      std::cout<<i<<" "<<pi[i]<<" |z - pi| = "<< fabs(diff[i]) <<" < "<< (4./(double)(2*(i+n)+3)) << " " << times[i]<<"s "<<std::endl;
    }
    std::cout<<"Summary:"<<std::endl;
    std::cout<<"#Size  n    |  Avg. Time (s) |   Min. Time(s) |   Max. Time(s) | σ Time(s)"<<std::endl;
    std::cout<< n <<" "<< timeAvg <<" "<< tmin <<" "<< tmax <<" "<< sig << std::endl;
    std::cout<<"Last inner product: "<<pi[nSamples-1]<<std::endl;
    
    return 0;
  
  }