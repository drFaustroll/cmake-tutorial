#include <iostream>

int print_stats(int n, int nSamples, double *pi, double *diff){

    std::cout<<"#Loop  |       Size|        pi|       diff| bound|"<<std::endl;
    for(int i=0; i<nSamples; ++i) {
      std::cout<<i+1<<" "<<n+i << " "<<pi[i]<<" "<< fabs(diff[i]) <<" < "<< (4./(double)(2*(i+n)+3)) << std::endl;
    }
    std::cout<<"Last value of pi: "<<pi[nSamples-1]<<std::endl;
    return 0;

  }
