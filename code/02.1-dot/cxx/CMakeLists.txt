cmake_minimum_required(VERSION 3.10)
cmake_policy(SET CMP0048 NEW)

project(testdot LANGUAGES CXX
  VERSION 0.1.0.1)

set(CMAKE_CXX_STANDARD 11)

set(AUTHOR "Alin Elena;Edoardo Pasca")
set(AUTHOR_DETAILS "alin-marin.elena@stfc.ac.uk;edoardo.pasca@stfc.ac.uk")
set(DESCRIPTION "simple dot product example")

message(STATUS "building ${PROJECT_NAME}, version ${PROJECT_VERSION}")
message(STATUS "major version ${testdot_VERSION_MAJOR}")
message(STATUS "minor version ${testdot_VERSION_MINOR}")

add_executable(testdot
  src/algebra.cxx
  src/testdot.cxx
  )
target_include_directories(testdot PRIVATE ${PROJECT_SOURCE_DIR}/include)
