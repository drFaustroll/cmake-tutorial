cmake_policy(SET CMP0048 NEW)

cmake_minimum_required(VERSION 3.10)

project(testdot LANGUAGES Fortran
       VERSION 0.1.0.1)

set(AUTHOR "Alin Elena;Edoardo Pasca")
set(AUTHOR_DETAILS "alin-marin.elena@stfc.ac.uk;edoardo.pasca@stfc.ac.uk")
set(DESCRIPTION "simple dot product example")
option(BUILD_DOCS "Build with API Docs" OFF)
option(BUILD_SHARED_LIBS "Build shared libraries" ON)

message(STATUS "building ${PROJECT_NAME}, version ${PROJECT_VERSION}")

include(GNUInstallDirs)

set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_BINDIR})

add_subdirectory(src)

if(BUILD_DOCS)
  find_package(Doxygen REQUIRED)
  configure_file(${PROJECT_SOURCE_DIR}/cmake/Doxyfile.cmake ${PROJECT_BINARY_DIR}/Doxyfile)
  add_custom_target(docs
    ${DOXYGEN_EXECUTABLE} ${PROJECT_BINARY_DIR}/Doxyfile)
endif()

# uninstall target
if(NOT TARGET uninstall)
    configure_file(
        "${CMAKE_CURRENT_SOURCE_DIR}/cmake/cmake_uninstall.cmake.in"
        "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
        IMMEDIATE @ONLY)

    add_custom_target(uninstall
        COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)
endif()

