#!/usr/bin/env bash

cd src
gfortran -o micro.x types.F90 constants.F90 setup.F90 parser.F90 readControl.F90 readField.F90 potentials.F90 neighbours.F90 useful.F90 units.F90 forces.F90 sampler.F90 ewald.F90  readConfig.F90 micro.F90

cp micro.x ../

