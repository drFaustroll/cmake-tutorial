#!/usr/bin/env bash

cd src

g++ utils.cpp solution.cpp reading.cpp parseCLI.cpp gutenberg.cpp generalinputs.cpp collectivevariable.cpp atom.cpp main.cpp -o mdsmplay -lboost_random -lboost_program_options -lboost_system

cp msdmplay ../

cd ../
