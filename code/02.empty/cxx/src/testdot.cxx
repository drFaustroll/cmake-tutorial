#define _USE_MATH_DEFINES // for C++
#include <iostream>
#include <limits>
#include <algebra.h>
#include <cmath>
#include <print_stats.h>

int main(int argc, char **argv){

  if (argc != 3) {
    std::cout<<"Wrong number of arguments!!!"<<std::endl;
    std::cout<<"Usage "<<argv[0]<<" n NSample"<<std::endl;
    return -1;
  }


  int n=atoi(argv[1]);
  int nSamples=atoi(argv[2]);
  std::cout<<"Running... ";
  for(int i=0; i<argc; ++i) {
    std::cout<<argv[i]<<" ";
  }
  std::cout<<std::endl;

  int N = n+nSamples;
  double *A = new double[N];
  double *B = new double[N];

  double *pi = new double[nSamples];
  double *diff = new double[nSamples];

  double z;

  /*********************************************************/
  //                  set arrays values
  /*********************************************************/
  int sign = 1;
  for (int j=0; j<N; ++j) {
    A[j] = 4./(double)(2*j+1);
    B[j] = (double)(sign);
    sign *= -1;
  }
  /*********************************************************/

  for (int i=0; i<nSamples; ++i) {
    z = dDot(n+i, A, 1, B, 1);
    pi[i] = z;
    diff[i] = M_PI - z;
  }

  print_stats(n, nSamples, pi, diff);


  delete[] pi;
  delete[] diff;
  delete[] A;
  delete[] B;

  return 0;
}
