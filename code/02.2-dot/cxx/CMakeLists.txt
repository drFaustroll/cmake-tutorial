cmake_policy(SET CMP0048 NEW)

cmake_minimum_required(VERSION 3.10)

project(testdot LANGUAGES CXX
  VERSION 0.1.0.1)

set(CMAKE_CXX_STANDARD 11)

set(AUTHOR "Alin Elena;Edoardo Pasca")
set(AUTHOR_DETAILS "alin-marin.elena@stfc.ac.uk;edoardo.pasca@stfc.ac.uk")
set(DESCRIPTION "simple dot product example")

message(STATUS "building ${PROJECT_NAME}, version ${PROJECT_VERSION}")

include(GNUInstallDirs)

set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_BINDIR})

add_subdirectory(src)
