cmake_policy(SET CMP0048 NEW)

cmake_minimum_required(VERSION 3.10)

project(testdot LANGUAGES CXX
       VERSION 0.1.0.1)

set(AUTHOR "Alin Elena;Edoardo Pasca")
set(AUTHOR_DETAILS "alin-marin.elena@stfc.ac.uk;edoardo.pasca@stfc.ac.uk")
set(DESCRIPTION "simple dot product example")
option(BUILD_DOCS "Build with API Docs" OFF)
option(BUILD_SHARED_LIBS "Build shared libraries" ON)
option(ENABLE_OPENMP "Allow user to activate/deactivate OpenMP support" ON)
option(ENABLE_BLAS "Allow user to activate openblas if available" OFF)

if (ENABLE_OPENMP)
  include(FindOpenMP)
  if(OpenMP_CXX_FOUND)
    message(STATUS "OpenMP for C++ Compiler Found, version ${OpenMP_CXX_VERSION_MAJOR}.${OpenMP_CXX_VERSION_MINOR}")
  else()
    message(ERROR_CRITICAL "No OpenMP support detected")
  endif()
endif()

if (ENABLE_BLAS)
  # if pkg-config is installed by conda then one needs to set the path to it
  # export PKG_CONFIG_PATH=${CONDA_PREFIX}/lib/pkgconfig:$PKG_CONFIG_PATH
  find_package(PkgConfig)
  if (PkgConfig_FOUND)
    pkg_check_modules(openblas openblas>=0.3)
    if (openblas_FOUND)
      set(BLAS_LIBRARIES ${openblas_LINK_LIBRARIES})
      set(BLAS_INCLUDEDIR ${openblas_INCLUDEDIR})
    endif()
  else()
    # try to find without pkg-config
    # this mode requires the BLAS location and includes to be available in default locations:
    # with GCC this means you need to set LD_LIBRARY_PATH to the library path and
    # CPATH or C_INCLUDE_PATH and CPLUS_INCLUDE_PATH to the default include path
    set(BLA_VENDOR OpenBLAS)
    find_package(BLAS REQUIRED)
  endif()
endif()

message(STATUS "building ${PROJECT_NAME}, version ${PROJECT_VERSION}")

include(GNUInstallDirs)

set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_BINDIR})

add_subdirectory(src)

if(BUILD_DOCS)
  find_package(Doxygen REQUIRED)
  configure_file(${PROJECT_SOURCE_DIR}/cmake/Doxyfile.cmake ${PROJECT_BINARY_DIR}/Doxyfile)
  add_custom_target(docs
    ${DOXYGEN_EXECUTABLE} ${PROJECT_BINARY_DIR}/Doxyfile)
endif()

# uninstall target
if(NOT TARGET uninstall)
    configure_file(
        "${CMAKE_CURRENT_SOURCE_DIR}/cmake/cmake_uninstall.cmake.in"
        "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
        IMMEDIATE @ONLY)

    add_custom_target(uninstall
        COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)
endif()

#packaging

set(CPACK_GENERATOR "RPM;DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "John Smith")
include(CPack)

message(STATUS "CXX_COMPILE_FEATURES ${CMAKE_CXX_COMPILE_FEATURES}")
