#include <algebra.h>
#ifdef _OPENMP
#include <omp.h>
#endif

double dot(const double* A, const double* B, const int n){

  double s=0.0;

#pragma omp parallel for reduction(+:s)
  for(int i=0; i<n; ++i) {
    s += A[i]*B[i];
  }
  return s;
}

double dDot(const int n, const double * A, const int stride_a, const double * B , const int stride_b){

  return dot (A, B, n);

}
