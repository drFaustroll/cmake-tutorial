# cmake-tutorial

- what is cmake?
- why cmake?
1) C++/Fortran file build to executable
    simple workflow

    cmake -SProjectSource -BmyBuild && cmake --build myBuild

2) simple hello: dot produt openmp
      - minimum requirements
      - compiler, versioning
      - folders,
      - create library,
      - options
      - configure file, api documentation (custom targets)
      - installation, uninstall, packaging

3) More advanced:
     - compiler features selection
     - conditional compiling
     - external libraries (pkgconfig)
     - testing, ctest
     - ExternalProject_Add
     - mixed languages (C/Fortran/CUDA)

4) use cmake-gui, ccmake

slides https://drfaustroll.gitlab.io/cmake-tutorial/#/title-slide

slides.pdf exist may not be up to date since are manually added for lectures.
